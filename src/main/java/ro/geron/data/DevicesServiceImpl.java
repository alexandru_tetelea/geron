package ro.geron.data;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import ro.geron.core.DevicesRepository;
import ro.geron.core.DevicesService;
import ro.geron.data.models.Device;

@Component
@AllArgsConstructor
public class DevicesServiceImpl implements DevicesService {
    private final DevicesRepository repository;

    @Autowired
    public DevicesServiceImpl(DevicesRepository repository) {
        this.repository = repository;
    }

    @Override
    public Iterable<Device> getAllDevices() {
        return repository.findAll();
    }

    @Override
    public Device save(Device device) {
        return repository.save(device);
    }

    @Override
    public Iterable<Device> findAll(PageRequest pageRequest) {
        return repository.findAll(pageRequest);
    }

    @Override
    public Iterable<Device> findAll() {
        return repository.findAll();
    }


}
