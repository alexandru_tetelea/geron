package ro.geron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeronApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeronApplication.class, args);
    }
}
