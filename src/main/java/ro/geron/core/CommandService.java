package ro.geron.core;

public interface CommandService<T, R> {
    public R execute(T command);
}
