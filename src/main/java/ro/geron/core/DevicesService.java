package ro.geron.core;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ro.geron.data.models.Device;

@Service
public interface DevicesService {
    public Iterable<Device> getAllDevices();

    public Device save(Device device);

    Iterable<Device> findAll(PageRequest pageRequest);

    Iterable<Device> findAll();
}
