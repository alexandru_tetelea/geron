package ro.geron.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ro.geron.core.CommandService;
import ro.geron.data.models.Device;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Component
@Scope(SCOPE_PROTOTYPE)
public class AddDeviceCommand implements CommandService<Device, Void> {
    private final WebSocketService webSocketService;

    @Autowired
    public AddDeviceCommand(WebSocketService webSocketService) {
        this.webSocketService = webSocketService;
    }

    @Override
    public Void execute(Device command) {
        webSocketService.send(command);
        return null;
    }
}
