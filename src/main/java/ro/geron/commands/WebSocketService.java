package ro.geron.commands;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.geron.data.models.Device;

@Slf4j
@Service
class WebSocketService {

    private final SimpMessagingTemplate template;

    @Autowired
    public WebSocketService(SimpMessagingTemplate template) {
        this.template = template;
    }

    public <T extends Device> void send(T device) {
        template.convertAndSend("/global-message/update", device);
    }

    public <T extends Device> void sendRemove(T device) {
        template.convertAndSend("/global-message/remove", device);
    }

}
