package ro.geron.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.geron.core.ProcessTextCommand;

@RestController
@RequestMapping("/command")
public class CommandApi {
    ProcessTextCommand textCommand;

    @Autowired
    public CommandApi(ProcessTextCommand textCommand) {
        this.textCommand = textCommand;
    }

    @PostMapping
    public void excuteCommand(@RequestBody Command command) {
        textCommand.process(command.value);
    }
}
